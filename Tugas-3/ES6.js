//No.1 - Mengubah fungsi menjadi fungsi arrow
console.log('NO 1:')
var golden = () => {
    console.log("this is golden!!")
    }
    golden()
console.log("")

//No.2 - Sederhanakan menjadi Object literal di ES6
console.log('NO 2:')
newFunction = (firstName, lastName) => { 
    firstName 
    lastName 
    return{
        fullName(){
            console.log(firstName + " " + lastName)
        }
    }
}
 //Driver Code
 newFunction("William", "Imoh").fullName()
 console.log("")

 //No.3 - Destructuring
 console.log('NO 3:')
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
    }
const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation, spell)
console.log("")

//No.4 - Array Spreading
console.log('NO 4:')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combinedArray = [...west, ...east]
//Driver Code
console.log(combinedArray)
console.log("")

//No.5 - Template Literals
console.log('NO 5:')
const planet = "earth"
const view = "glass"
var before = `Lorem ${view}  dolor sit amet, consectetur adipiscing elit,  ${planet} do eiusmod tempor  
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log(before);
console.log("")


