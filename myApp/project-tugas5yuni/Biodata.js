import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/IMG_20160702_150920.jpg')} />
                    </View>
                    <Text style={styles.name}>Yuni Nurtikasari</Text>
                </View>
                <View style={styles.status}>
                    <Text style={styles.study}>Program study : </Text>
                        <Text style ={styles.state}>Teknik Informatika</Text>
                </View>
                <View style={styles.class}>
                    <Text style={styles.kelas}>kelas : </Text>
                    <Text style ={styles.state}>Pagi C</Text>
                </View>
                <View style={styles.instagram}>
                    <Text style={styles.ig}>Instagram :</Text>
                    <Text style ={styles.state}>yunins__</Text>
                </View>
                <View style={styles.telegram}>
                    <Text style={styles.tele}>telegram :</Text>
                    <Text style ={styles.state}>YuniNs</Text>
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: 'yellow',
        flex:1,
        width: 500
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 100,
        height: 100,
        borderRadius: 50
    },
    name: {
        color: 'black',
        marginTop: 15,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 40,
        bottom: 20
    },
    stat: {
        alignItems: 'center',
        flex : 1
    },
    statAmount:{
        color:'#4F566D',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 20,
        color: 'black',
        fontWeight: '300',
        marginTop: 4
    },
    status: {
        bottom: -20,
        left: 100
    },
    study:{
        justifyContent: 'flex-end',
        color: 'black',
        fontSize: 15,
        fontWeight: 'bold',
    },
    class: {
        bottom:-20,
        left: 100
    },
    kelas:{
        fontWeight: 'bold',
        fontSize : 15
    },
    instagram: {
        bottom : -20,
        left : 100
    },
    ig: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    state : {
        fontSize : 25,
        right : 100,
        bottom :-20
    }
});