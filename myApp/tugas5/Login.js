import * as React from 'react';
import { Text, View, StyleSheet, Image, TextInput } from 'react-native';
import { Constants } from 'expo';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  render() {
    return (
      <View style={{ 
        height: 100 + '%',
        width: 100 + '%',
        backgroundColor: 'rgb(219, 112, 147)',
        justifyContent: 'center',
        alignItems: 'center',

      }}> 

      <Text style =
      {{
        fontSize: 55,
        color: "Black",

      }}
      >LOGIN
      </Text>

      <Text style =
      {{
        fontSize: 30,
        color: "Black",

      }}
      >Sistem Akademik
      </Text>
     <Image source = {require('./assets/LOGIN.png') }
    style = {{ 
      backgroundColor: 'rgba(255, 255, 255, 0.5)',
      height: 88,
      width: 88,
      borderRadius: 44,
      marginTop: 18, 

      }}
      
  
   />
      <TextInput
      placeholder = "username"
      placeholderTextColor = "Black"
      style = {{
      backgroundColor: 'rgba(255, 255, 255, 0.5)',
      width: 60 + '%',
      padding: 18,
      borderRadius: 18,
      marginTop: 18,

      }}
       />
          <TextInput
      placeholder = "password"
      placeholderTextColor = "Black"
      style = {{
      backgroundColor: 'rgba(255, 255, 255, 0.5)',
      width: 60 + '%',
      padding: 18,
      borderRadius: 18,
      marginTop: 18,

      }}
       />
        <Text style = 
        {{
          fontSize: 66,
          color: 'white',
          marginTop: 2
     
        }}
      />
      <TextInput
      placeholder = "Login"
      placeholderTextColor= "Black"
      style = {{
      backgroundColor: 'rgba(255, 255, 255, 0.5)',
      width: 13 + '%',
      padding: 2,
      borderRadius: 14,
      marginTop: 18,

      }}

       />
        <Text style =
      {{
        fontSize: 10,
        color: "white",

      }}
      >Forget Password?
      </Text>
      </View>
    );
  }
}