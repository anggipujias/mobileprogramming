import * as React from 'react';
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native';
import { Constants } from 'expo';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  render() {
    return (
      <View style={{ 
        height: 100 + '%',
        width: 100 + '%',
        backgroundColor: 'rgb(219, 112, 147)',
        justifyContent: 'center',
        alignItems: 'center',

      }}> 
      <Image source = {require('./assets/logo.png') }
        style = {{ 
          backgroundColor: 'rgba(255, 255, 255, 0.5)',
          height: 88,
          width: 88,
          borderRadius: 44,
          marginTop: 18, 

      }}
       
   />

      <Text style =
      {{
        marginBottom: 0,
        fontSize: 35,
        color: "Black",

      }}
      >Welcome
      </Text>

      <Text style =
      {{
        fontSize: 30,
        color: "Black",

      }}
      >Sistem Akademik
      </Text>


     <Text style =
      {{
        marginTop:20,
        marginBottom:0,
        fontSize:8,
        color: "white",

      }}
      >please select SIGN IN,
      </Text>
     

     <Text style =
      {{
        marginBottom:-10,
        fontSize:8,
        color: "white",

      }}
      >if you already have an account
      </Text>
      
      <TouchableOpacity style={{
        height: 40,
        width: 220,
        borderRadius: 25,
        fontSize: 16,
        padding: 15,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        justifyContent: 'center',
        marginTop: 20,
        }}>
          <Text style={{
            color: 'rgba(255, 255, 255, 70)',
            fontWeight: "bold",
            fontSize: 14,
            textAlign: 'center'
          }}>SIGN IN</Text>
      </TouchableOpacity>

       <Text style =
      {{
        marginTop:20,
        marginBottom:10,
        fontSize:11,
        color: "white",

      }}
      >OR
      </Text>

       <Text style =
      {{
        fontSize:8,
        color: "white",

      }}
      >Please select SIGN UP,
      </Text>

      <Text style =
      {{
        marginBottom:-10,
        fontSize:8,
        color: "white",

      }}
      >if you don't have an account
      
      </Text>
      
      <TouchableOpacity style={{
        height: 40,
        width: 220,
        borderRadius: 25,
        fontSize: 16,
        padding: 15,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        justifyContent: 'center',
        marginTop: 20,
        }}>
          <Text style={{
            color: 'rgba(255, 255, 255, 70)',
            fontWeight: "bold",
            fontSize: 14,
            textAlign: 'center'
          }}>SIGN UP</Text>
      </TouchableOpacity>

      </View>
    );
  }
}