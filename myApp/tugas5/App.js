import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import HalamanUtamaScreen from './HalamanUtama'
import LoginScreen from './Login';
export default class App extends Component {
  render() {
    return (
      <View>
        <HalamanUtamaScreen />
      </View>
    )
  }
};
